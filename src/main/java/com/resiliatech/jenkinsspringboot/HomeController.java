package com.resiliatech.jenkinsspringboot;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by alfred on 27 October 2020
 */
@RestController
@RequestMapping("/jenkins")
public class HomeController {
    @GetMapping
    public String hello() {
        return "Hello from jenkins";
    }
    @GetMapping("/alfred")
    public String helloAlfred(Model model) {
        model.addAttribute("alfred", "alfred");
        model.addAttribute("alfred", "alfred");
        model.addAttribute("alfred", "alfred");
        return "Hello from alfred";
    }
    @GetMapping("/lin")
    public String helloLin(Model model) {
        model.addAttribute("lin", "lin");
        model.addAttribute("lin", "lin");
        model.addAttribute("lin", "lin");
        return "Hello from Lin";
    }
    @GetMapping("/an")
    public String helloAn(Model model) {
        model.addAttribute("an", "an");
        model.addAttribute("an", "an");
        model.addAttribute("an", "an");
        return "Hello from An";
    }
    @GetMapping("/j")
    public String helloJ(Model model) {
        model.addAttribute("j", "j");
        model.addAttribute("j", "j");
        model.addAttribute("j", "j");
        return "Hello from J";
    }
    @GetMapping("/john")
    public String helloJohn(Model model) {
        model.addAttribute("john", "john");
        model.addAttribute("john", "john");
        model.addAttribute("john", "john");
        return "Hello from John";
    }
    @GetMapping("/kuda")
    public String helloKuda(Model model) {
        model.addAttribute("kuda", "kuda");
        model.addAttribute("kuda", "kuda");
        model.addAttribute("kuda", "kuda");
        return "Hello from Kuda";
    }
    @GetMapping("/peter")
    public String helloPeter(Model model) {
        model.addAttribute("peter", "peter");
        model.addAttribute("peter", "peter");
        model.addAttribute("peter", "peter");
        return "Hello from Peter";
    }
}
