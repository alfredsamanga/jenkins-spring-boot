package com.resiliatech.jenkinsspringboot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by alfred on 27 October 2020
 */
@WebMvcTest(HomeController.class)
public class HomeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testHello() throws Exception {
        mockMvc.perform(get("/jenkins"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello")));
    }

    @Test
    public void testHelloalfred() throws Exception {
        mockMvc.perform(get("/jenkins/alfred"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello")));
    }

    @Test
    public void testHellolin() throws Exception {
        mockMvc.perform(get("/jenkins/lin"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello")));
    }

    @Test
    public void testHelloan() throws Exception {
        mockMvc.perform(get("/jenkins/an"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello")));
    }

    @Test
    public void testHelloj() throws Exception {
        mockMvc.perform(get("/jenkins/j"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello")));
    }

    @Test
    public void testHellojohn() throws Exception {
        mockMvc.perform(get("/jenkins/john"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello")));
    }

    @Test
    public void testHellokuda() throws Exception {
        mockMvc.perform(get("/jenkins/kuda"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello")));
    }

    @Test
    public void testHellopeter() throws Exception {
        mockMvc.perform(get("/jenkins/peter"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello")));
    }
}
